# Turkish translation for gmpc
# Copyright (c) 2009 Rosetta Contributors and Canonical Ltd 2009
# This file is distributed under the same license as the gmpc package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: gmpc\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2009-03-16 16:28+0100\n"
"PO-Revision-Date: 2009-10-12 08:54+0000\n"
"Last-Translator: Bora Akbay <boraakbay@boraakbay.info>\n"
"Language-Team: Turkish <tr@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2009-11-16 23:32+0000\n"
"X-Generator: Launchpad (build Unknown)\n"

#. Name
#: ../src/plugin.c:64
msgid "Alarm Timer"
msgstr "Alarm Zamanlayıcısı"

#: ../src/plugin.c:343
msgid "_Enable alarm"
msgstr "Alarmı Mümkün Kıl"

#. Set up the alarm time spinners
#: ../src/plugin.c:349
msgid "Time:"
msgstr "Saat:"

#: ../src/plugin.c:375
msgid "Action:"
msgstr "Eylem:"

#. 
#. * Hard-coded possible actions for once alarm deadline hit
#. * TODO: Allow customized actions?
#. 
#: ../src/plugin.c:385
msgid "Play/Pause"
msgstr "Oynat/Beklet"

#: ../src/plugin.c:386
msgid "Stop"
msgstr "Durdur"

#: ../src/plugin.c:387
msgid "Close  (& Stop)"
msgstr "Kapat (ve Durdur)"

#: ../src/plugin.c:388
msgid "Close only"
msgstr "(Sadece Kapat)"

#: ../src/plugin.c:389
msgid "Shutdown"
msgstr "Kapat"

#: ../src/plugin.c:390
msgid "Toggle random"
msgstr ""

#. Label to show remaining time
#: ../src/plugin.c:395
msgid "Time left:"
msgstr "Kalan zaman:"
