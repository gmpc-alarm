# Japanese translation for gmpc
# Copyright (c) 2009 Rosetta Contributors and Canonical Ltd 2009
# This file is distributed under the same license as the gmpc package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: gmpc\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2009-03-16 16:28+0100\n"
"PO-Revision-Date: 2009-03-22 17:59+0000\n"
"Last-Translator: Yuki Kodama <Unknown>\n"
"Language-Team: Japanese <ja@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2009-11-16 23:32+0000\n"
"X-Generator: Launchpad (build Unknown)\n"

#. Name
#: ../src/plugin.c:64
msgid "Alarm Timer"
msgstr "アラームタイマー"

#: ../src/plugin.c:343
msgid "_Enable alarm"
msgstr "アラームを有効にする"

#. Set up the alarm time spinners
#: ../src/plugin.c:349
msgid "Time:"
msgstr "時間:"

#: ../src/plugin.c:375
msgid "Action:"
msgstr "動作:"

#. 
#. * Hard-coded possible actions for once alarm deadline hit
#. * TODO: Allow customized actions?
#. 
#: ../src/plugin.c:385
msgid "Play/Pause"
msgstr "再生/一時停止"

#: ../src/plugin.c:386
msgid "Stop"
msgstr "停止"

#: ../src/plugin.c:387
msgid "Close  (& Stop)"
msgstr "閉じる (&s)"

#: ../src/plugin.c:388
msgid "Close only"
msgstr "閉じるのみ"

#: ../src/plugin.c:389
msgid "Shutdown"
msgstr "シャットダウン"

#: ../src/plugin.c:390
msgid "Toggle random"
msgstr "ランダムにする"

#. Label to show remaining time
#: ../src/plugin.c:395
msgid "Time left:"
msgstr "経過時間:"
