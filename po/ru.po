# Russian translation for gmpc
# Copyright (c) 2009 Rosetta Contributors and Canonical Ltd 2009
# This file is distributed under the same license as the gmpc package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: gmpc\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2009-03-16 16:28+0100\n"
"PO-Revision-Date: 2009-05-06 08:17+0000\n"
"Last-Translator: julja <ljashechka-ju@mail.ru>\n"
"Language-Team: Russian <ru@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2009-11-16 23:32+0000\n"
"X-Generator: Launchpad (build Unknown)\n"

#. Name
#: ../src/plugin.c:64
msgid "Alarm Timer"
msgstr "Таймер напоминания"

#: ../src/plugin.c:343
msgid "_Enable alarm"
msgstr "_Включить напоминание"

#. Set up the alarm time spinners
#: ../src/plugin.c:349
msgid "Time:"
msgstr "Время:"

#: ../src/plugin.c:375
msgid "Action:"
msgstr "Действие:"

#. 
#. * Hard-coded possible actions for once alarm deadline hit
#. * TODO: Allow customized actions?
#. 
#: ../src/plugin.c:385
msgid "Play/Pause"
msgstr "Воспроизведение/Пауза"

#: ../src/plugin.c:386
msgid "Stop"
msgstr "Остановить"

#: ../src/plugin.c:387
msgid "Close  (& Stop)"
msgstr "Закрыть (и остановить)"

#: ../src/plugin.c:388
msgid "Close only"
msgstr "Только закрыть"

#: ../src/plugin.c:389
msgid "Shutdown"
msgstr "Выключить"

#: ../src/plugin.c:390
msgid "Toggle random"
msgstr "Случайное воспроизведение"

#. Label to show remaining time
#: ../src/plugin.c:395
msgid "Time left:"
msgstr "Осталось времени:"
